#include "MyObj.h"
#include <QKeyEvent>
#include <QDebug>


MyObj::MyObj () {
	setFlag(QGraphicsItem::ItemSendsGeometryChanges);
}

MyObj::~MyObj() {
	delete(this);
}
void MyObj::paintEvent(QGraphicsScene * scene){

	scene->addItem(this);
	
}
/*
void MyObj::keyPressEvent (QKeyEvent *event){

    if (event->key() == Qt::Key_Left){
	if (pos().x() <= 0 ) {qDebug() << "Edge";}
	else {setPos(x()-20,y());}
    }
    else if (event->key() == Qt::Key_Right){
	if (pos().x() >= 800) {qDebug() << "Edge";}
        else {setPos(x()+20,y());}
    }
    else if (event->key() == Qt::Key_Up){
	if (y() <= 0) {qDebug() << "Edge";}
        else {setPos(x(),y()-20);}
    }
    else if (event->key() == Qt::Key_Down){
	if (y() >= 500) {qDebug() << "Edge";}
        else {setPos(x(),y()+20);}
    }

}*/
void MyObj::keyPressEvent (QKeyEvent *event){

    if (event->key() == Qt::Key_Left){
	setPos(x()-20,y());
    }
    else if (event->key() == Qt::Key_Right){
	setPos(x()+20,y());
    }
    else if (event->key() == Qt::Key_Up){
	setPos(x(),y()-20);
    }
    else if (event->key() == Qt::Key_Down){
	setPos(x(),y()+20);
    }

}

QVariant MyObj::itemChange ( GraphicsItemChange change, const QVariant & value )
{
    if (change == ItemPositionChange && scene()) {
        // value is the new position.
        QPointF newPos = value.toPointF();
        QRectF rect = scene()->sceneRect();
        if (!rect.contains(newPos)) {
            // Keep the item inside the scene rect.
            newPos.setX(qMin(rect.right(), qMax(newPos.x(), rect.left())));
            newPos.setY(qMin(rect.bottom(), qMax(newPos.y(), rect.top())));
            return newPos;
        }
    }
    return QGraphicsItem::itemChange(change, value);
}
