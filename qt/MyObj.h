#ifndef MYOBJ_H
#define MYOBJ_H

#include <QGraphicsRectItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QVariant>
#include <QKeyEvent>

//Class MyObj hinerate from QGraphicsRectItem or QGraphicsPixmapItem

class MyObj : public QGraphicsPixmapItem {

public:
	MyObj();

	~MyObj ();

	void paintEvent(QGraphicsScene * scene);
	void keyPressEvent (QKeyEvent * event);
	QVariant itemChange(GraphicsItemChange change, const QVariant & value);
	
};

#endif

