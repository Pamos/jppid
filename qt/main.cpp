#include <QApplication>
#include <QWidget>
#include <QtGui>
#include <QtGui>
#include <QPixmap>
#include <iostream>
#include <QMainWindow>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QCoreApplication>
#include "MyObj.h"


int main (int argc, char ** argv) {

	QApplication a(argc, argv);
	
	//create Objects
	MyObj * widget = new MyObj();
	MyObj * widgett = new MyObj();
	QPixmap pix(75,75);
	//QPixmap pixx(75,75);
	widget->setPixmap(pix);
	//widgett->setPixmap(pixx);
	//MyObj * widget2 = new MyObj();
	//widget2->setRect(10,0,50,50);

	//create scene
	QGraphicsScene * scene = new QGraphicsScene();
	
	//putting Objectes onto the scene
	widget->paintEvent(scene);
	//widgett->paintEvent(scene);

	//make Object focusable
	widget->setFlag(QGraphicsItem::ItemIsFocusable);
    	widget->setFocus();

	//creating Grphical view
	QGraphicsView * view = new QGraphicsView (scene);
	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);	//remove the scrollBars from the pop up
	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	view->show();
	view->setFixedSize(800,600);	//fixing an automatic size
	view->setSceneRect(10,0,800,600);

	widget->setPos(0,0);
	widgett->setPos(100,100);
	

	return a.exec();
}
