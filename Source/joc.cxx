#include "../Headers/Joc.hxx"
#include <windows.h>


#define ENEMS 7
#define FLY_E 5
#define TOWERS 4
#define ENEMIES ENEMS+FLY_E+TOWERS

//#include "../Headers/MyWidget.hxx"

Joc * joc;

int main(int argc, char *argv[]) {
    int x = 100, y = 100;
    //agafem mides de la pantalla
    try {
        setup(&x, &y);
    } catch (int e) {
        std::cout << "Error geting size of the window\n";
        exit(0);
    }
    //instruccions
    srand(time(NULL));
    int at = 0;
    std::cout << "START\n";
    std::cout << "Press space to start\n";
    std::cout << "\n\n\n";
     std::cout << "-----------------------------------------\n";
    std::cout << "MOVE: \n";
    std::cout << "RIGHT ARROW key\n";
    std::cout << "LEFT ARROW key\n";
    std::cout << "DOWN ARROW key\n";
    std::cout << "UP ARROW  key\n\n";
    std::cout << "-----------------------------------------\n";
    std::cout << "ATACK:  UP w  DOWN s  LEFT a  RIGHT d\n";
    std::cout << "-----------------------------------------\n\n";
    std::cout << "ENEMIES: \n";
    std::cout << "E -> basic enemies, can't move through mountains and water and if they touch you they kill you!!!!\n"; 
    std::cout << "F -> flying enemies, can move through mountains and if they touch you they kill you!!!!\n"; 
    std::cout << "T -> tower enemies, they can't move but they will shot!!!!\n"; 
    while (!GetAsyncKeyState(VK_SPACE));
    
    int sd = rand() % 1000 + 200;
    int seed = rand() % 1000 + 200;
    joc = new Joc(x - 1, y - 1, sd);

    //iniciem el joc
    while (joc->run()) {
        joc->checkEnemies();
    }
     std::cout << "T -> tower enemies, they can't move but they will shot!!!!\n"; 
    joc->HDRupdate(); 
    
    exit(0);
}
