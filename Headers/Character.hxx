#include <cstdlib>
#include <iostream>
#include <windows.h>
#include <vector>
#include "Map.hxx"
#include "MyUtils.hxx"
#include <mutex>
#include<time.h>



using namespace std;

#ifndef CHARACTER_H
#define CHARACTER_H

#ifdef __cplusplus
extern "C" {
#endif

#define RANGEDDAMAGE 100
#define TOWERDAMAGE 40

#define RED 12
#define GREEN 10
#define WHITE 15
#define CIAN 11
#define PURP 13

    mutex mtx;
    mutex mtx1;

    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    //superclasse entitat per a qualsevol esser movil del joc

    class Entity {
    protected:
        int hp;
        int position[2];
        char Img;//Caràcter que representa l'entitat
        int previous;// informació sobre que hi havia en la posició en la que esteim
        int cnt;//Frequencia de dispar de bales de la torre
        int solid;//El seu valor com a colisio
        int color;//color en que es printa
    public:

        Entity(int HP, int posX, int posY, char img) {
            hp = HP;
            Img = img;
            position[0] = posX;
            position[1] = posY;
            cnt = 0;
        }

        char getImg() {
            return Img;
        }

        //setter de vida

        void setHP(int HP) {
            hp = HP;
        }

        //getter de vida

        int getHP() {
            return hp;
        }

        //getter de la posicio actual de l'entitat

        int* getPos() {
            return position;
        }

        //funcio per moure al personatge a una posicio

        void move(int posX, int posY) {
            position[0] = posX;
            position[1] = posY;
        }



        //funcio que modifica la vida d'una entitat
        //en funcio d'una cantitat de mal

        virtual void damage(int dmg) {

            if (dmg >= hp) {
                hp = 0;
            } else {
                hp -= dmg;
            }
        }
		//printer de totes les entitats
        virtual void printer(Map *m) {
            int**solids = m->coliders();
            mtx.lock();
            SetConsoleTextAttribute(hConsole, color);
            gotoxy(position[0], position[1]);
            cout << Img;
			//Canvia la posició del mapa de solids en solid i no es pot atrevesasr
            solids[position[1]][position[0]] = solid;
            SetConsoleTextAttribute(hConsole, WHITE);
            mtx.unlock();


        }
		//Borra l'entitat de la seva posició anterior
        virtual void eraser(Map *m) {
            char ** map = m->getMap();
            int** solids = m->coliders();
			//Deixa el mapa com estava abans de pasr-hi
            if (hp <= 0)solids[position[1]][position[0]] = previous;
            mtx.lock();
            gotoxy(position[0], position[1]);
            cout << map[position[1]][position[0]];
            mtx.unlock();


        }

        int getCnt() {
            return cnt;
        }

        void cntSum() {
            cnt++;
        }

    };

    class Jugador : public Entity {
    protected:
        int faced;//diracció en la que mira

    public:

        Jugador(int HP, int posX, int posY, char im) : Entity(HP, posX, posY, im) {

            solid = 3;
            color = GREEN;

        }

        virtual int* attack(int d) = 0;
		//Mou el jugador en la direcció indicada
        bool movePlayer(Map*m, int dir) {

            int ** solids = m->coliders();

            int posX = position[0];
            int posY = position[1];
            eraser(m);
            calcMove(&posX, &posY, dir);
			//si no colisiona amb ningu avança
            if (!m->colide(posX, posY)) {
                solids[position[1]][position[0]] = 0;
                move(posX, posY);
                printer(m);
                return true;
            }

            printer(m);
            return false;

        }
		//En el cas del personatge si mor acaba el programa
        void damage(int dmg) {
            if (dmg > hp) {
                mtx1.lock();
                hp = 0;
                ClearScreen();
                std::cout <<"GAME OVER";
                exit(0);
                mtx1.unlock();

            } else {
                mtx1.lock();
                hp -= dmg;
                mtx1.unlock();
            }
        }

        int getFacing() {
            return faced;
        }



    };

    class Enemy : public Entity {
    protected:
        int id;
        Jugador*pj;
        int rond;

    public:

        Enemy(Jugador * p, int HP, int posX, int posY, char img, int ID) : Entity(HP, posX, posY, img) {
            pj = p;
            rond = 0;
            solid = 5;
            id = ID;
            color = RED;
        }

        ~Enemy() {
            delete pj;
        }

        int getId() {
            return id;
        }

        //funcio per moure l'enemic

        virtual bool moveEnemy(Map*m) {
            int* posP = pj->getPos();
            int x = position[0];
            int y = position[1];

            eraser(m);

            int** solids = (m)->coliders();
            int resX = x, resY = y;
            //#pragma omp parallel for
            for (int i = 0; i < 8; i++) {
                x = position[0];
                y = position[1];
                //actualitza les cordenades segons una direccio i tal i compare
                //l'hem definit previament
                mtx.lock();
                calcMove(&x, &y, i);
                if (!(m)->colide(x, y) || m->pColide(x, y)) {
                    mtx.unlock();
                    if (dis(x, y, posP[0], posP[1]) < dis(resX, resY, posP[0], posP[1])) {
                        resX = x;
                        resY = y;
                    }
                } else {
                    mtx.unlock();
                }

            }
			//si toca el jugador li fa mal
            if (resX == posP[0] && resY == posP[1]) {
                pj->damage(10);
                printer(m);

            }//si no toca el jugador avança 
			else {
                solids[position[1]][position[0]] = previous;
                move(resX, resY);
                previous = solids[resY][resX];
                printer(m);
            }
            return true;
        }
    };

    class Bullet {
    private:
        int posX;
        int posY;
        int dir;
        int dmg;
        char Img;
        char previous;
        bool destroy;
        int color;
    public:

        Bullet(int x, int y, int d, char im, int damage) : dmg(damage) {
            posX = x;
            posY = y;
            dir = d;
            Img = im;
            printer();

        }

        ~Bullet() {
        }
		//funció que mou les bales de la torre
        bool move(Map*m, Jugador *pj) {
            int x = posX;
            int y = posY;
            color = PURP;
            eraser(m);
            calcMove(&posX, &posY, dir);

            int* pos = pj->getPos();
			//comprova si fa mal al personatge
            if (pos[0] == posX && pos[1] == posY) {
                pj->damage(dmg);
                posX = x;
                posY = y;
                this->eraser(m);
                return false;
            }
			//comprova si colisiona
            if (m->colide(posX, posY)&&!m->wColide(posX, posY)) {
                posX = x;
                posY = y;
                this->eraser(m);
                return false;
            }
            this->printer();
            return true;
        }
		//funció que mou les bales del jugador
        bool move(Map*m, vector<Enemy*> & aux) {
            int x = posX;
            int y = posY;
            color = CIAN;
            eraser(m);
            mtx.lock();
            calcMove(&posX, &posY, dir);
			//si colisiona amb un enemic li fa mal
            if (m->eColide(posX, posY) || m->fColide(posX, posY)) {
                for (int j = 0; j < aux.size(); j++) {
                    int* pos = aux[j]->getPos();
                    if (pos[0] == posX && pos[1] == posY) {
                        aux[j]->damage(dmg);
                        mtx.unlock();
                        posX = x;
                        posY = y;
                        eraser(m);
                        return false;
                    } else {
                        mtx.unlock();
                    }
                }

            } else {
                mtx.unlock();
            }
			//si ha de colisionar amb un solid retorna true i la funció move bullet l'elimina
            if (m->colide(posX, posY)&&!m->wColide(posX, posY)&&!m->pColide(posX, posY)) {
                posX = x;
                posY = y;
                eraser(m);
                return false;
            }

            printer();
            return true;
        }

        int getPosX() {
            return posX;
        }

        int getPosY() {
            return posY;
        }

        void setPrev(char prev) {
            previous = prev;
        }

        char getImg() {
            return Img;
        }

        char getPrev() {
            return previous;
        }

        bool getDes() {
            return destroy;
        }
		//printer de les bales
        virtual void printer() {

            mtx.lock();
            SetConsoleTextAttribute(hConsole, color);
            gotoxy(posX, posY);
            cout << Img;
            SetConsoleTextAttribute(hConsole, WHITE);
            mtx.unlock();
        }
		//Borra les bales del mapa
        virtual void eraser(Map *m) {
            char ** map = m->getMap();
            mtx.lock();
            gotoxy(posX, posY);
            cout << map[posY][posX];
            mtx.unlock();

        }


    };

    class RangedPlayer : public Jugador {
    private:
        vector<Bullet *> bullets;
        double start;//Serveix per calcular distancia entre bales
        double end;
    public:

        RangedPlayer(int HP, int posX, int posY) : Jugador(HP, posX, posY, '&') {
            start = 0;
            end = 0;
        }

        ~RangedPlayer() {
            for (int i = 0; i < bullets.size(); i++) {
                delete bullets[i];
            }
        }
		//funcio que controla la freqüència de dispars
        bool shot() {
            if (start == 0 || start > 1000) {
                start = clock();
                return true;
            } else {

                end = clock();
                if (end - start > 1000) {
                    start = 0;
                }
                return false;
            }
        }
		//funció que crea la bala
        int* attack(int d) {
            faced = d;
            if (shot()) {

                switch (faced) {

                        //eix y
                    case 0://Atac dalt
                        bullets.push_back(new Bullet(position[0], position[1], 0, '^', RANGEDDAMAGE));

                        break;
                    case 2://Atac dreta
                        bullets.push_back(new Bullet(position[0], position[1], 2, '>', RANGEDDAMAGE));

                        break;
                    case 4://Atac baix
                        bullets.push_back(new Bullet(position[0], position[1], 4, 'v', RANGEDDAMAGE));

                        break;
                    case 6://Atac esquerra
                        bullets.push_back(new Bullet(position[0], position[1], 6, '<', RANGEDDAMAGE));

                        break;
                    default:

                        break;
                        return NULL;
                }
            }
        }
		//funcio encarregada de moure i eliminar les bales
        void moveBullets(Map* m, vector<Enemy*>& enemies) {
            for (int i = 0; i < bullets.size(); i++) {
                if (!bullets[i]->move(m, enemies)) {
                    bullets[i]->eraser(m);
                    bullets.erase(bullets.begin() + i);
                }

            }




        }
    };

    class Tower : public Enemy {
    private:
        vector<Bullet *> bullets;
    public:

        Tower(Jugador * p, int HP, int posX, int posY, int ID) : Enemy(p, HP, posX, posY, 'T', ID) {
            int cnt = 0;
        }

        ~Tower() {
            for (int i = 0; i < bullets.size(); i++) {
                delete bullets[i];
            }
        }

        void Shot() {//disperem 4 bales
            if (cnt % 40 == 0) {
                //disperem eix y
                bullets.push_back(new Bullet(position[0], position[1] - 1, 0, '+', TOWERDAMAGE));
                bullets.push_back(new Bullet(position[0], position[1] + 1, 4, '+', TOWERDAMAGE));
                //disperem eix x
                bullets.push_back(new Bullet(position[0] + 1, position[1], 2, '+', TOWERDAMAGE));
                bullets.push_back(new Bullet(position[0] - 1, position[1], 6, '+', TOWERDAMAGE));
            }
            cnt++;
        }
		//funcio que crea i mou les bales
        bool moveEnemy(Map*m) {
            //espai entre bales
            printer(m);
            this->Shot();

            for (int i = 0; i < bullets.size(); i++) {
                if (!bullets[i]->move(m, pj)) bullets.erase(bullets.begin() + i);
            }
            return true;

        }
    };

    class Fly_enemy : public Enemy {
    private:
    public:

        Fly_enemy(Jugador * p, int HP, int posX, int posY, int ID) : Enemy(p, HP, posX, posY, 'F', ID) {
            pj = p;
            solid = 4;
            previous = 0;
        }


        //funcio per moure l'enemic, en aquest cas te menys restriccions ja que pot pasar per sobre l'aigua i les muntanyes

        bool moveEnemy(Map*m) {
            int* posP = pj->getPos();
            int x = position[0];
            int y = position[1];

            eraser(m);

            int** solids = (m)->coliders();
            int resX = x, resY = y;
            for (int i = 0; i < 8; i++) {
                x = position[0];
                y = position[1];
                //actualitza les cordenades segons una direccio i tal i compare
                //l'hem definit previament
                mtx.lock();
                calcMove(&x, &y, i);
                if (!m->fColide(x, y)&&!m->eColide(x, y)) {
                    mtx.unlock();
                    if (dis(x, y, posP[0], posP[1]) < dis(resX, resY, posP[0], posP[1])) {
                        resX = x;
                        resY = y;

                    }
                } else {
                    mtx.unlock();
                }

            }


            if (resX == posP[0] && resY == posP[1]) {
                pj->damage(10);
                printer(m);

            } else {
                solids[position[1]][position[0]] = previous;
                move(resX, resY);
                previous = solids[resY][resX];
                printer(m);
            }
            return true;
        }
    };


#ifdef __cplusplus
};
#endif

#endif /* CHARACTER_H */

