#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <time.h>

#include "Perlin.hxx"


#define THREADS 8

#ifndef _MAP
#define _MAP

class Map {
private:
    int** solids; //matriu de cossos solids del mapa per on no poden passar les entitats
    char** _map; //matriu grafica del mapa
    int r; //rows
    int c; //columns
    bool next;
public:
    //constructor amb les dimensions definides

    Map(int row, int col) {
        r = row;
        c = col;
        allocMatrix(row, col);

    }
    //constructor amb les dimensions per defecte

    Map() {
        r = 100;
        c = 50;
        allocMatrix(100, 50);

    }

    ~Map() {
        free(solids);
        free(_map);
    }

    //funcio auxiliar per allotjar memoria er una matriu
    //implementada paral·lelament per millorar els temps de generacio

    void allocMatrix(int row, int col) {
        solids = (int **) malloc(row * sizeof (int*));
        _map = (char **) malloc(row * sizeof (char*));

#pragma omp parallel for
        for (int i = 0; i < row; i++) {
            solids[i] = (int *) malloc(col * sizeof (int));
            _map[i] = (char *) malloc(col * sizeof (char));
        }
    }

    //funcio auxiliar per inicialitzar el mapa
    //defineix solids a les fronteres d'aquest

    void initMat() {

        int i, j, num;
#pragma omp parallel private(i, j) num_threads(THREADS)
        {
#pragma omp for schedule(static)
            for (i = 0; i < r; i++) {
                for (j = 0; j < c; j++) {
                    if (i == 0 || j == 0 || i == r - 1 || j == c - 1) {
                        solids[i][j] = 1;
                    } else {
                        solids[i][j] = 0;
                    }
                }
            }
        }
    }

    //Funcio auxiliar que comprova si hi ha un cami recte
    //sense cap col·lisio entre dos punts

    bool freePath(int x0, int y0, int x1, int y1)//line Bresenham algortihm
    {
        int dx, dy;
        int stepx, stepy;
        dx = x1 - x0;
        dy = y1 - y0;

        if (dy < 0) {
            dy = -dy;
            stepy = -1;
        } else {
            stepy = 1;
        }
        if (dx < 0) {
            dx = -dx;
            stepx = -1;
        } else {
            stepx = 1;
        }

        dy <<= 1; /* dy is now 2*dy */
        dx <<= 1; /* dx is now 2*dx */

        if ((0 <= x0) && (x0 < r) && (0 <= y0) && (y0 < c))solids[x0][y0] = 1;

        if (dx > dy) {
            int fraction = dy - (dx >> 1);

            while (x0 != x1) {
                x0 += stepx;
                if (fraction >= 0) {
                    y0 += stepy;
                    fraction -= dx;
                }
                fraction += dy;
                if ((0 <= x0) && (x0 < r) && (0 <= y0) && (y0 < c))solids[x0][y0] = 1;
            }

        } else {
            int fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                if ((0 <= x0) && (x0 < r) && (0 <= y0) && (y0 < c)) {
                    if (solids[x0][y0] == 1)return false;
                }
            }
        }
        return true;
    }


    /*
    from 0 to 0.35 we’ll have water or lakes
    from 0.35 to 0.6 we’ll have floor or planes
    from 0.6 to 0.8 we’ll have walls or mountains
    from 0.8 to 1.0 we’ll have snow
     */

    //generem un mapa utilitzant perlin noise

    void gen_Map(const unsigned int &seed) {
        // El creem segons una llavor
        PerlinNoise pn(seed);
        int i, j;
#pragma omp parallel private(i, j) num_threads(THREADS)
        {
#pragma omp for schedule(static)
            for (i = 0; i < r; ++i) { // y
                for (j = 0; j < c; ++j) { // x
                    double x = (double) j / ((double) c);
                    double y = (double) i / ((double) r);

                    double n = pn.noise(10 * x, 10 * y, 0.8);

                    // Aigua
                    if (n < 0.35) {
                        _map[i][j] = '~';
                        solids[i][j] = 2;
                    }// Terra
                    else if (n >= 0.35 && n < 0.65) {
                        _map[i][j] = 178;
                    }// parets/muntanyes
                    else if (n >= 0.65 && n < 0.8) {
                        _map[i][j] = '#';
                        solids[i][j] = 1;
                    }// neu
                    else {
                        _map[i][j] = 'S';
                        solids[i][j] = 1;
                    }

                    if (i == r - 1 || j == c - 1)solids[i][j] = 1;
                }
            }
        }
    }

    //funció que comprova si en una posició hi ha un solid

    bool colide(int j, int i) {
        if (solids[i][j] == 0)return false;
        return true;
    }
    //funcio que comprova si en una posicio hi ha aigua

    bool wColide(int j, int i) {
        if (solids[i][j] == 2)return true;
        return false;
    }
    //funcio que comprova si en una posicio hi ha el personatge

    bool pColide(int j, int i) {
        if (solids[i][j] == 3)return true;
        return false;
    }
    //funcio que comprova si en una posicio hi ha enemic volador

    bool fColide(int j, int i) {
        if (solids[i][j] == 4)return true;
        return false;
    }
    //funcio que comprova si en una posicio hi ha enemic

    bool eColide(int j, int i) {
        if (solids[i][j] == 5)return true;
        return false;
    }

    //funcio que retorna el mapa de col·lisions

    int** coliders() {
        return solids;
    }

    //funcio que retorna el mapa grafic

    char** getMap() {
        return _map;
    }

    bool getNext() {
        return next;
    }

    void setNext(bool b) {
        next = b;
    }

    //funcio que mostra per pantalla el mapa de col·lisions

    void writeColision() {
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                std::cout << " " << solids[i][j] << " ";
            }

            std::cout << "\n";
        }

    }

    //funcio que imprimeix transicio entre nivells

    void loading() {

        std::cout << "Press space to start\n";
        char ca = 219;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                std::cout << ca;
            }

            std::cout << "\n";
        }
    }

    //funcio que mostra per pantalla el mapa grafic

    void writeMap() {
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                std::cout << _map[i][j];
            }

            std::cout << "\n";
        }
    }

};

#endif





