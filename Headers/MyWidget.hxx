#ifndef MyWidget_hxx
#define MyWidget_hxx

#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QtGui>
#include <QTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QtGui>
#include <iostream>

class MyWidget : public QWidget {
    int _xPosition;
    int _yPosition;
    int _height;
    int _width;
public:

    MyWidget(int pos_x, int pos_y, int height, int witdh) {

        _xPosition = pos_x;
        _yPosition = pos_y;
        _height = height;
        _width = witdh;

        QPalette palette(MyWidget::palette());
        palette.setColor(backgroundRole(), Qt::white);
        setPalette(palette);
    }

    ~MyWidget() {
        delete(this);
    }

    int getXPosition() {
        return _xPosition;
    }

    int getYPosition() {
        return _yPosition;
    }

    int getHeight() {
        return _height;
    }

    int getWidth() {
        return _width;
    }

    void paintEvent(QPaintEvent *) {

        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen(Qt::black);
        painter.drawRect(this->getXPosition(), this->getYPosition(), this->getHeight(), this->getWidth());
    }

    void quitScreen() {
        this->close();
    }
};
