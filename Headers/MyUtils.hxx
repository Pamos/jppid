#ifndef _MYUTILS
#define _MYUTILS

/*Funcions auxiliars polivalents*/
#include <cmath>
#include <windows.h>

int offset; //variable encarregada d'enregistrar el desplaçament del mapa segons la capçalera del joc


//funcio auxiliar per obtenir la distancia entre dos punts P1 i P2
double dis(int x1, int y1, int x2, int y2) {
    double dis = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
    //cout << "\nDis: " << dis << "\n";
    return dis;
}


//Funcio auxiliar encarregada de calcular una nova posicio a partir de la posicio anterior i la direccio
//La posicio nova sera igual al moviment en una unitat en la direccio definida pel seguent codi:
//
//				8(NW)	1(N)	2(NE)
//					      |   
//				7(W)  ----+----	3(E)
//					      |   
//				6(SW)	5(S)	4(SE)

void calcMove(int*posX, int*posY, int dir) {
    switch (dir) {
        case 0:
            *posY -= 1;
            break;
        case 1:
            *posX += 1;
            *posY -= 1;
            break;
        case 2:
            *posX += 1;
            break;
        case 3:
            *posX += 1;
            *posY += 1;
            break;
        case 4:
            *posY += 1;
            break;
        case 5:
            *posX -= 1;
            *posY += 1;
            break;
        case 6:
            *posX -= 1;
            break;
        case 7:
            *posX -= 1;
            *posY -= 1;
            break;
        default:
            break;
    }
}


//Funcio auxiliar inversa a l'anterior. A partir de dues posicions et retorna la direccio
//de la segona respecte la primera
int getDir(int Xa, int Ya, int Xb, int Yb) {
    int x, y;
    x = Xb - Xa;
    y = Yb - Ya;

    if (x == 0 && y < 0)return 0;
    if (x > 0 && y < 0)return 1;
    if (x > 0 && y == 0)return 2;
    if (x > 0 && y > 0)return 3;
    if (x == 0 && y > 0)return 4;
    if (x < 0 && y > 0)return 5;
    if (x < 0 && y == 0)return 6;
    if (x < 0 && y < 0)return 7;
}


//Funcio auxiliar per esborrar tot el que hi ha impres a la terminal
void ClearScreen() {
    HANDLE hStdOut;
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    DWORD count;
    DWORD cellCount;
    COORD homeCoords = {0, 0};

    hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hStdOut == INVALID_HANDLE_VALUE) return;

    /* Obtenim la mida del buffer de la consola*/
    if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
    cellCount = csbi.dwSize.X * csbi.dwSize.Y;

    /* omplim el buffer d'espais */
    if (!FillConsoleOutputCharacter(
            hStdOut,
            (TCHAR) ' ',
            cellCount,
            homeCoords,
            &count
            )) return;

    /* Omplim el buffer amb els elements del format desitjat */
    if (!FillConsoleOutputAttribute(
            hStdOut,
            csbi.wAttributes,
            cellCount,
            homeCoords,
            &count
            )) return;

    /* tornem a la posixio (0,0) */
    SetConsoleCursorPosition(hStdOut, homeCoords);
}


//Funcio auxiliar per posicionar el cursor de la consola a un punt desitjat tenint en compte l'offset
void gotoxy(int column, int line) {
    COORD coord;
    coord.X = column;
    coord.Y = line + offset;
    SetConsoleCursorPosition(
            GetStdHandle(STD_OUTPUT_HANDLE),
            coord
            );
}


//Funcio auxiliar per posicionar el cursor de la consola a un punt desitjat sense tenir en compte l'offset
void nGotoxy(int column, int line) {
    COORD coord;
    coord.X = column;
    coord.Y = line;
    SetConsoleCursorPosition(
            GetStdHandle(STD_OUTPUT_HANDLE),
            coord
            );
}


//Funcio auxiliar que imprimeix un requadre a la part superior de la consola
int HDR(int size) {

    char h = '-';
    char v = '|';

    gotoxy(0, 0);

    for (int i = 0; i < size; i++) {
        std::cout << h;
    }

    gotoxy(0, 1);
    std::cout << v;
    gotoxy(size - 1, 1);
    std::cout << v;

    gotoxy(0, 2);
    std::cout << v;
    gotoxy(size - 1, 2);
    std::cout << v;

    gotoxy(0, 3);
    std::cout << v;
    gotoxy(size - 1, 3);
    std::cout << v;

    gotoxy(0, 4);
    for (int i = 0; i < size; i++) {
        std::cout << h;
    }


    return 5;
}


//Funcio auxiliar per detectar les mides de la consola i convertirla a pantalla completa
//tambe pren mides de l'offset i esborra tot el que hi havia anteriorment en pantalla
void setup(int*columns, int*rows) {
	/*netejem la pantalla*/
    ClearScreen();
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	/*canviem el color de les lletres a blanc*/
    SetConsoleTextAttribute(hConsole, 15);


    HWND console = GetConsoleWindow();
    RECT r;

	/*prenem les mides del monitor*/
    *columns = GetSystemMetrics(SM_CXSCREEN);
    *rows = GetSystemMetrics(SM_CYSCREEN);
    if (*rows < 0 || *columns < 0) {
        throw 0;
        return;
    }

	/*situem la consola a la posicio 0,0 i la fem de les mides del monitor*/
    GetWindowRect(console, &r);
    MoveWindow(console, r.left, r.top, *columns, *rows, TRUE);

    CONSOLE_SCREEN_BUFFER_INFO csbi;

	/*enregistrem la mida del buffer*/
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    *columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    *rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;

    if (*rows < 0 || *columns < 0) {
        throw 0;
        return;
    }
	/*actualitzem l'offset*/
    offset = HDR(*columns);
    *rows -= offset;
    gotoxy(0, 0);

}

#endif