#ifndef JOC_H
#define JOC_H

#define BULLET_V 3

#include <time.h>
#include "../Headers/Character.hxx"
#include "../Headers/Map.hxx"
#include "MyUtils.hxx"
#include <iostream>
#include <thread>
#include <vector>
#include <chrono>
#include <windows.h>

using namespace std;


#ifdef __cplusplus
extern "C" {
#endif

    class Joc {
    private:
        vector<Enemy *> enemies; //Vector que conte els enemics
        vector<thread > tids;//Vector que cont� el threads que mouen els enemics
        vector <thread > mtids; //Vectror que cont� els threads que creen el mapa
        vector<thread > player;//Vector de threads que mouen el persontatge 
        mutex mut;//Mutex per els locks
        int e, f, t;//numero d'enemic, enemics voladors i torretes
        Jugador * pj; //jugador
        Map * m; //mapa
        Map *aux;//Mapa auxiliar que ens serveix en diverses funcions de creaci� de mapes
        Map *m1;//Mapa auxiliar que ens serveix en diverses funcions de creaci� de mapes
        int mr, mc; //dimensions del mapa
        int score;
        int round;
        int seed;
        bool makeMap;//Ens indica si esteim creant el seguent mapa

    public:

        Joc(int mx, int my, int s) {
            seed = s;
            mr = my;
            mc = mx;
            m1 = new Map(mr, mc);
            aux = new Map(mr, mc);

            pj = new RangedPlayer(100, mc / 2, mr / 2);

            score = 0;
            round = 0;

            //numero de enemics
            e = 0;
            f = 0;
            t = 0;

            makeMap = true;

            //creem mapa
            aux->initMat();
            aux->gen_Map(seed);
            m1->initMat();
            m1->gen_Map(seed);

            m1->setNext(true);
            creatMap();
            newMap();

        }

        ~Joc() {

            for (int i = 0; i < tids.size(); i++) {
                delete enemies[i];
            }
        }
        //fa un thread per inicialitzar el mapa

        void creatMap() {
            if (makeMap) {
                mtids.push_back(thread(&Joc::threadMap, this));

            }
        }

        //inicialitzem el mapa seguent

        void threadMap() {
            if (aux->getNext()) {
                aux->initMat();
                aux->gen_Map(seed);
            } else if (m1->getNext()) {
                m1->initMat();
                m1->gen_Map(seed);
            }

        }
        //cargem el mapa del nou nivell

        void newMap() {


            if (round > 0)player.back().join();

            //mira quin es el mapa del seg�ent nivell
            if (aux->getNext()) {
                aux->setNext(false);
                m1->setNext(true);
                m = aux;
            } else if (m1->getNext()) {
                m1->setNext(false);
                aux->setNext(true);
                m = m1;
            }

			//augmenta el nombre d'enemics de la ronda
            int x = mc / 2, y = mr / 2;
            e += rand() % 5 + 1;
            f += rand() % 2;
            t += rand() % 2;
            m->initMat();
            m->gen_Map(seed);


			//Comprova que el personatge no colisiona i el mou a aquella posicio
            while (m->colide(x, y)) {
                x = rand() % (mc - 1);
                y = rand() % (mr - 1);
            }
            //cout<<x<<" "<<y;
            pj->move(x, y);

            gotoxy(0, 0);
            m->loading();
            pj->printer(m);
            while (!GetAsyncKeyState(VK_SPACE));

            gotoxy(0, 0);
            m->writeMap();

			//Genera els diversos enemics
            genHorde(e);
            genFly_Horde(f);
            genTowers(t);
			//Crea el thread del jugador per aquesta ronda
            player.push_back(thread(&Joc::live, this));
			//Crea els threads dels enemics i espera a que tots finalitzin
            for (int i = 0; i < enemies.size(); i++) {
                tids.push_back(thread(&Joc::moveEnemy, this, i));
            }

            for (int i = 0; i < tids.size(); i++) {
                tids[i].join();
            }

            tids.clear();
            enemies.clear();
            round++;
            makeMap = true;

        }
		//Printa el header del joc
        void HDRupdate() {
            mtx.lock();
            nGotoxy(4, 2);

            cout << "          ";

            nGotoxy(4, 2);
            cout << "HP: " << pj->getHP();

            nGotoxy(40, 2);

            cout << "          ";

            nGotoxy(40, 2);

            cout << "Score: " << score;

            nGotoxy(80, 2);

            cout << "                    ";

            nGotoxy(80, 2);

            cout << "Enemies: " << enemies.size();
            mtx.unlock();
        }

		//Funcio que s'executa en el thread del personatge i encarregade de moure i atacar
        void live() {
            while (enemies.size() > 0 && pj->getHP() > 0) {

                if (GetAsyncKeyState(0x57)&&!GetAsyncKeyState(0x44)&&!GetAsyncKeyState(0x41)) {

                    pjAttack(0);

                }
                //abaix
                if (GetAsyncKeyState(0x53)&&!GetAsyncKeyState(0x44)&&!GetAsyncKeyState(0x41)) {


                    pjAttack(4);

                }
                //esquerra
                if (GetAsyncKeyState(0x41)&&!GetAsyncKeyState(0x57)&&!GetAsyncKeyState(0x53)) {

                    pjAttack(6);

                }
                //dreta
                if (GetAsyncKeyState(0x44)&&!GetAsyncKeyState(0x57)&&!GetAsyncKeyState(0x53)) {

                    pjAttack(2);

                }

                //movem al personatge segons les tecles
                if (GetAsyncKeyState(VK_UP)&&!GetAsyncKeyState(VK_RIGHT)&&!GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(0);
                }
                if (GetAsyncKeyState(VK_DOWN)&&!GetAsyncKeyState(VK_RIGHT)&&!GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(4);
                }
                if (GetAsyncKeyState(VK_LEFT)&&!GetAsyncKeyState(VK_UP)&&!GetAsyncKeyState(VK_DOWN)) {
                    movePlayer(6);
                }
                if (GetAsyncKeyState(VK_RIGHT)&&!GetAsyncKeyState(VK_UP)&&!GetAsyncKeyState(VK_DOWN)) {
                    movePlayer(2);
                }
                if (GetAsyncKeyState(VK_UP) && GetAsyncKeyState(VK_RIGHT)&&!GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(1);
                }
                if (GetAsyncKeyState(VK_UP)&&!GetAsyncKeyState(VK_RIGHT) && GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(7);
                }
                if (GetAsyncKeyState(VK_DOWN) && GetAsyncKeyState(VK_RIGHT)&&!GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(3);
                }
                if (GetAsyncKeyState(VK_DOWN)&&!GetAsyncKeyState(VK_RIGHT) && GetAsyncKeyState(VK_LEFT)) {
                    movePlayer(5);
                }
                if (GetAsyncKeyState(VK_ESCAPE)) {
                    pj->setHP(0);
                    exit(0);
                }
                for (int i = 0; i < BULLET_V; i++) {
                    pjMoveBullet();
                }
                pj->printer(m);

                HDRupdate();
				//Feim un sleep per evitar mourernos massa r�pid
                this_thread::sleep_for(chrono::milliseconds(90));
            }
        }

		//Crea el vector d'enemics normal
        void genHorde(int n) {

            //Definim la posicio dels enemics comprovant que no
            //coincideixi amb cap solid
            int x = 0, y = 0;
            for (int i = 0; i < n; i++) {
                do {
                    x = rand() % (mc - 1);
                    y = rand() % (mr - 1);
                } while (m->colide(x, y));
                enemies.push_back(new Enemy(pj, 100, x, y, 'E', i));


            }

        }
		//Crea el vector d'enemics voladors
        void genFly_Horde(int n) {

            //Definim la posicio dels enemics comprovant que no
            //coincideixi amb cap solid
            int x = 0, y = 0;
            for (int i = 100; i < n + 100; i++) {
                do {
                    x = rand() % (mc - 1);
                    y = rand() % (mr - 1);
                } while (m->colide(x, y));
                enemies.push_back(new Fly_enemy(pj, 200, x, y, i));
            }
        }
		//Crea el vector de torres
        void genTowers(int n) {

            //Definim la posicio dels enemics comprovant que no
            //coincideixi amb cap solid
            int x = 0, y = 0;
            for (int i = 1000; i < n + 1000; i++) {
                do {
                    x = rand() % (mc - 1);
                    y = rand() % (mr - 1);
                } while (m->colide(x, y));
                Tower *t = new Tower(pj, 500, x, y, i);
                t->printer(m);
                enemies.push_back(t);
            }
        }
		//Mou les bales del ersonatge
        void pjMoveBullet() {
			//El cast seria necessari si s'implementes l'enemic mele
            if (RangedPlayer * t = dynamic_cast<RangedPlayer*> (pj)) {
                t->moveBullets(m, enemies);
            }

        }

        //Funcio que mou al personatge segons una direccio
        //les direccions venen definides segons els punts cardinals
        //ordenats numericament en sentit horari. Sent aixi el nord 0 i el nord-oest 7

        bool movePlayer(int dir) {

            pj->movePlayer(m, dir);

        }

        //Fa que l'enemic en la posicio i del vector es mogui
        //segons el seu algorisme definit

        void moveEnemy(int i) {
            int value = enemies[i]->getId();
			//cream un auxiliar per ja que com que anirem canviant el numero d'enemics la i tambe varia
            Enemy* auxiliar = enemies[i];
            while (auxiliar->getHP() > 0) {
                auxiliar->moveEnemy(m);
                this_thread::sleep_for(chrono::milliseconds(200));
            }
			//En morir l'enemic de la llista
            for (std::vector<Enemy*>::iterator iter = enemies.begin(); iter != enemies.end(); ++iter) {
                if ((*iter)->getId() == value) {
                    (*iter)->eraser(m);
                    mut.lock();
                    enemies.erase(iter);
                    mut.unlock();
                    break;
                }
            }
            score += 10;
            HDRupdate();
        }
		//Printa el mapa i el personatge
        void writeMap() {
            m->writeMap();
            pj->printer(m);

        }
		//Comprova que el personatge estigui viu
        bool run() {
            if (pj->getHP() > 0)return true; 
            return false;
        }
		//el personatge ataca
        void pjAttack(int d) {
            pj->attack(d);
        }
		//Funcio que s'executa sempre desde el main
        bool checkEnemies() {
			//Quan arribem a un cer nombre d'enemics comen�a a crear el seg�ent mapa
            if (enemies.size()<(e + f + t) / 2 + 1) {
                creatMap();
                makeMap = false;
            }
			//quan no queden enemics crida a la funci� newMap per canviar de ronda
            if (enemies.size() == 0) {
                mtids.back().join();
                seed = 2 * seed;
                newMap();

            }
        }

    };




#ifdef __cplusplus
};
#endif

#endif /* JOC_H */



